package com.example.peterson.proyecto;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.peterson.proyecto.Requests.UserLoginRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private CardView btnInicio, btnRegistro;
    private EditText usuario, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        final SharedPreferences sharedPreferences = getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);

        btnInicio = findViewById(R.id.cardView);
        btnRegistro = findViewById(R.id.cardView2);
        usuario = findViewById(R.id.usuario);
        password = findViewById(R.id.contraseña);

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String user = usuario.getText().toString();
                final String pass = password.getText().toString();
                final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, "Procesando", "Por favor espere...",
                        false, false);
                if (!user.isEmpty() && !pass.isEmpty()) {
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                int code = jsonResponse.getInt("code");
                                String data = jsonResponse.getString("data");
                                if (code == 200) {
                                    loading.dismiss();
                                    JSONObject jsonArray = jsonResponse.getJSONObject("data");
                                    Intent i = new Intent(LoginActivity.this, PrincipalActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putInt("id", jsonArray.getInt("id"));
                                    editor.putString("email", jsonArray.getString("email"));
                                    editor.putString("nombre", jsonArray.getString("nombre"));
                                    editor.putString("pass", jsonArray.getString("pass"));
                                    editor.apply();
                                } else {
                                    loading.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                    builder.setMessage(data)
                                            .setNegativeButton("Retry", null)
                                            .create().show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    UserLoginRequest userLoginRequest = new UserLoginRequest(user, pass, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                    queue.add(userLoginRequest);
                } else {
                    Toast.makeText(LoginActivity.this, "Ingrese todos los datos.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }
}
