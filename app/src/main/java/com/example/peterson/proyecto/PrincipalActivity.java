package com.example.peterson.proyecto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PrincipalActivity extends AppCompatActivity {

    private List<String> categorias;
    private GridView gridView;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        sharedPreferences = getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        gridView = findViewById(R.id.gridView);

        categorias = new ArrayList<String>();
        categorias.add("Categoria 1");
        categorias.add("Categoria 2");
        categorias.add("Categoria 3");

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent i = new Intent(PrincipalActivity.this, CategoriesListActivity.class);
                startActivity(i);
            }
        });

        MyAdapter myAdapter = new MyAdapter(this, R.layout.grid_item, categorias);
        gridView.setAdapter(myAdapter);

        SharedPreferences sharedPreferences = getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        int id = sharedPreferences.getInt("id", 0);
        String email = sharedPreferences.getString("email", "");
        String nombre = sharedPreferences.getString("nombre", "");
        String pass = sharedPreferences.getString("pass", "");
        Toast.makeText(this, "" + id + " " + nombre + " " + email + " " + pass, Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_logout:
                logOut();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void logOut(){
        Intent i = new Intent(PrincipalActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void removeSharedPreferences() {
        sharedPreferences.edit().clear().apply();
    }
}