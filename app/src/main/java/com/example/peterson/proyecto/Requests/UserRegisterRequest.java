package com.example.peterson.proyecto.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class UserRegisterRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "https://onlinemarketdyedp.000webhostapp.com/API_proyecto/index.php/add-user";

    private Map<String, String> params;

    public UserRegisterRequest(String nombre, String email, String pass, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("nombre", nombre);
        params.put("email", email);
        params.put("pass", pass);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
