package com.example.peterson.proyecto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class CategoriesListActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.listView);

        names = new ArrayList<String>();
        names.add("Item 1");
        names.add("Item 2");
        names.add("Item 3");
        names.add("Item 4");

        MyAdapter myAdapter = new MyAdapter(this,R.layout.list_item, names);
        listView.setAdapter(myAdapter);
    }
}
