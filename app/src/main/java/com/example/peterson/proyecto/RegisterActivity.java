package com.example.peterson.proyecto;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.peterson.proyecto.Requests.UserRegisterRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    private CardView btnContinuar;
    private EditText nombre, email, pass, pass2;
    private CheckBox stc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        btnContinuar = findViewById(R.id.cardView);
        stc = findViewById(R.id.switch1);
        nombre = findViewById(R.id.nombre);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        pass2 = findViewById(R.id.rpassword);

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = nombre.getText().toString();
                final String mail = email.getText().toString();
                final String password = pass.getText().toString();
                final String password2 = pass2.getText().toString();
                final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, "Procesando", "Por favor espere...",
                        false, false);

                if (stc.isChecked()) {
                    if (!name.isEmpty() && !mail.isEmpty() && !password.isEmpty()) {
                        if (password.equals(password2)) {
                            Response.Listener<String> responseListener = new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonResponse = new JSONObject(response);
                                        String data = jsonResponse.getString("data");
                                        int code = jsonResponse.getInt("code");

                                        if (code == 200) {
                                            loading.dismiss();
                                            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                                            startActivity(i);
                                        } else {
                                            loading.dismiss();
                                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                            builder.setMessage(data)
                                                    .setNegativeButton("Volver a intentar", null)
                                                    .create().show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            };

                            UserRegisterRequest userRegisterRequest = new UserRegisterRequest(name, mail, password, responseListener);
                            RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                            queue.add(userRegisterRequest);
                        } else {
                            Toast.makeText(RegisterActivity.this, "Las contraseñas deben ser iguales.", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(RegisterActivity.this, "Por favor llene todos los campos", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Por favor acepte los términos y condiciones para continuar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
