package com.example.peterson.proyecto.Requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class UserLoginRequest extends StringRequest {
    private static final String LOGIN_REQUEST_URL = "https://onlinemarketdyedp.000webhostapp.com/API_proyecto/index.php/login";

    private Map<String, String> params;

    public UserLoginRequest(String email, String pass, Response.Listener<String> listener) {
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("email", email);
        params.put("pass", pass);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
